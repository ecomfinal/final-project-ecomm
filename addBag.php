<!--Author: Reuben Larmie
	Date:11-04-2016
	Lecturer: Dr. Nathan Amanquah
-->

<!--Creating a form page to hold the parameters for administrator view side-->
<!DOCTYPE html>
  <html>
    <head>
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link type="text/css" rel="stylesheet" href="css1/materialize.min.css"  media="screen,projection"/>
      <meta name="viewport" content="width=device-width, initial-scale=2.0"/>
    </head>
    <body>
<nav>
    <div class="nav-wrapper amber darken-3">
       <a href="index.php" class="brand-logo">BEST BAG BUY Ltd</a>
      <ul id="nav-mobile" class="right hide-on-med-and-down">
<li><a href="admin/addBag.php">Add New Bag</a></li>
 <li><a href="admin/viewBag.php">View Bags</a></li>
 <li><a href="http://localhost/ecomfinal/admin/sortBag.php">Sort Bags By Order</a></li>
 <li><a href="http://localhost/ecomfinal/admin/browseBagByCat.php">Browse Bags By Category</a></li>
 <li><a href="http://localhost/ecomfinal/admin/searchBag.php">Search Bags</a></li>
 <li><a href="http://localhost/ecomfinal/admin/purchaseReport.php">Purchase Report</a></li>
 <li><a href="http://localhost/ecomfinal/phoneLogin.php">Logout</a></li>   
      </ul>
 </div> 
  </nav>

<html>
<head>
</head>
<body>
<!--Creating a form page to hold the parameters for administrator view side insertions-->
<form action="addBag.php" method="GET">
	<h1><center>Add New Bag Details</center></h1>
	<div>Bag ID    :<input type="text" name="bi"> </div><br>
	<div>Bag Name	:<input type="text" name="bn"> </div><br>
	<div>Bag Category	:<input type="text" name="bc"> </div><br>
	<div>Bag Year	:<input type="text" name="by"> </div><br>
	<div>Bag Brand ID	:<input type="text" name="bbi"> </div><br>
	<div>Description	:</div>
	<div> <textarea name="bdsc"	cols="30" rows="10"></textarea> </div>
	<div> <input type="submit" value="Add Bag" name="wa"> </div>
	
</form>

	<?php
		//if (isset($_REQUEST['bn'])) {
			//$server="localhost";
			//$database="sbags_db";
			//$user="root";
			//$password="";
	define ("DB_HOST", 'localhost');
	define ("DB_NAME", 'sbags_db');
	define ("DB_PORT", 3306);
	define ("DB_USER", "root");
	define ("DB_PWORD", "");
	$link =mysqli_connect(DB_HOST, DB_USER, DB_PWORD, DB_NAME);
	//Connecting to Server
	
	
	
	
	//Retrieving data from the form side
	$bag_id =$_REQUEST['bi'];
	$bag_name=$_REQUEST['bn'];
	$bag_cat=$_REQUEST['bc'];
	$year=$_REQUEST['by'];
	$bag_brand_id=$_REQUEST['bbi'];	
	$description=$_REQUEST['bdsc'];
	
	//Adding a new bag into the database
	$str_query= "INSERT into bag 
				SET bag_id = ?,	
				bag_name= ?,
				bag_cat= ?,
				year= ?,
				bag_brand_id= ?,
				description = ?";
		$stmt = $link->prepare($str_query);
		if ($stmt === false){
			echo "Sorry bruh, nothing".mysqli_error($link);
		} else{
		$stmt->bind_param('issiis',$bag_id,$bag_name,$bag_cat,$year,$bag_brand_id,$description);
		$anyname = $stmt->execute();
		echo "success";
		}
				
		//if (!mysql_query($str_query,$link)){
		//echo mysql_error();
		//exit();
		//}
		//echo "Bag Successfully Added";
	
	//Creating links to navigate to and from other administrator pages
	echo "<a href=\"searchBag.php?\">BACK TO SEACRH BAG PAGE || </a>";
	echo " ";
	echo " ";
	echo " ";
	echo "<a href=\"viewBag.php?\">BACK TO VIEW BAG PAGE || </a>";
	echo " ";
	echo " ";
	echo " ";
	echo "<a href=\"bagLogins.php?\">LOGOUT FROM BAG STORE</A>";
	
	?>
</body>
</html>

    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
      <script type="text/javascript" src="js1/materialize.min.js"></script>
    </body>
  </html>
