<?php

session_start();


define("DB_HOST", 'localhost');
define("DB_NAME", 'sbags_db');
define("DB_PORT", 3306);
define("DB_USER", "root");
define("DB_PWORD", "");

//try establish db connection
$mysqli = new mysqli(DB_HOST, DB_USER, DB_PWORD, DB_NAME);

//check connection
if ($mysqli->connect_error) {
    die("Connection Failed :: " . $mysqli->connect_error);
}

if (isset($_GET['bag_id']) && !isset($_GET['rmv'])){
	$bid = (int) $_GET['bag_id'];
	$sqlQuery = "SELECT bag_name, cost FROM bag_table where bag_id = $bid";
	$sqlStmt = $mysqli->prepare($sqlQuery);
	if ($sqlStmt === FALSE) {
		echo
		'Wrong SQL :: ' . $sqlQuery. 'Error:' . $mysqli->error;
		}
	$sqlStmt->execute();
	$sqlStmt->bind_result($tempName, $tempCost);
	if ($sqlStmt->fetch()){
		$bname = $tempName;
		$bcost = $tempCost;
	}
	
	if (!isset($_SESSION['bag_table'])){
		$_SESSION['bag_table'] = array ();
		$_SESSION['bag_table'][] = array ('bag_id' => $bid, 'bag_name' => $bname, 'cost' => $tempCost, 'count' =>1);
		
	} elseif (isset($_SESSION['bag_table'])){
		$increased = 0;
		foreach ($_SESSION['bag_table'] as &$item) {
			if ($item['bag_id'] === $bid) {
				$item['count'] ++;
				$increased = 1;
				break;
			}
		}
		unset ($item);
		if ($increased ===0){
			$_SESSION['bag_table'][] = array('bag_id' => $bid, 'bag_name' => $bname, 'cost' => $tempCost, 'count' => 1);
		}
	}
	$sqlStmt ->close();
}

$page = 1;
if (isset($_REQUEST['page'])){
	$page = $_REQUEST['page'];
}
header ("Location: index.php?page=".$page);