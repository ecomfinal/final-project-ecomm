<!--Author: Reuben Larmie
	Date:11-04-2016
	Lecturer: Dr. Nathan Amanquah
-->
<!DOCTYPE html>
  <html>
    <head>
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
      <meta name="viewport" content="width=device-width, initial-scale=2.0"/>
    </head>
    <body>
<nav>
    <div class="nav-wrapper amber darken-3">
<a href="../index.php" class="brand-logo">BEST BAG BUY Ltd</a>
      <ul id="nav-mobile" class="right hide-on-med-and-down">
<li><a href="addBag.php">Add New Bag</a></li>
 <li><a href="viewBag.php">View Bags</a></li>
 <li><a href="sortBag.php">Sort Bags By Order</a></li>
 <li><a href="browseBagByCat.php">Browse Bags By Category</a></li>
 <li><a href="searchBag.php">Search Bags</a></li>
 <li><a href="purchaseReport.php">Purchase Report</a></li>
 <li><a href="bagLogins.php">Logout</a></li>   
      </ul>
 </div> 
  </nav>
  
<!--Creating a searh text that will receive input for the purpose of seraching-->
<html>
		<head></head>
		<body>
		<form action="searchBag.php" method="GET">
		Search Text : <input type="text" size="10" name="st">
		<input type="submit" value="search"> 
		</form>
		<table>
		<?php
//Establishing local connection with server and database 
	include_once("errorhandler.php");
	ini_set('error_reporting', E_NOTICE);
	ini_set('error_reporting', E_ERROR);
	error_reporting(0);

	$server="localhost";
	$database="sbags_db";
	$user="root";
	$password="";
	$link=mysql_connect($server,$user,$password);
	if ($link==false){
	echo "error connecting to db";
	exit ();
	}
	if (!mysql_select_db($database,$link)) {
	echo "error selecting database";
	exit();
}
		//Initializing search text function that will facilitate the searching of key words using two or more letters
		$search_text= 0;
		if(isset($_REQUEST['st'])){
		$search_text=$_REQUEST['st'];
		$search_text= strip_tags($search_text);
	}
		//SQL query that will enable the generation of data from search function
		$str_query="select BAG_ID, BAG_NAME from bag where BAG_NAME like '%$search_text%'";
		$result=mysql_query($str_query, $link);
		if ($result==false){
		echo "<br>error running query</br>";
		echo mysql_error();
		exit();
	}
		//Placing data into a table with specified bag variable names
		echo "<div class=container>
		<table  class=centered>";
		echo "<tr style= 'background-color:orange; color:white; text-align:center'> <td> BAG ID</td><td>BAG Name</td></tr>";
		$row = mysql_fetch_assoc($result);
		while ($row){
		echo "<tr><td> {$row['BAG_ID']} </td>";
		echo "<td> {$row['BAG_NAME']} </td></tr>";
		$row = mysql_fetch_assoc($result);
	}
		echo "<tr> <td> ".$row['BAG_ID']." </td> <td style ='text-align:left'> ".$row['BAG_NAME']."</td></tr>";

		mysql_close($link);
		echo "</div></table>";
	?>
	</body>
</html>


      <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
      <script type="text/javascript" src="js/materialize.min.js"></script>
    </body>
  </html>
     