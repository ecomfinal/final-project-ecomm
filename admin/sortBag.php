<!--Author: Reuben Larmie
	Date:11-04-2016
	Lecturer: Dr. Nathan Amanquah
-->
<!DOCTYPE html>
    <html>
    <head>
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
      <meta name="viewport" content="width=device-width, initial-scale=2.0"/>
    </head>
    <body>
<nav>
    <div class="nav-wrapper amber darken-3">
    <a href="../index.php" class="brand-logo">BEST BAG BUY Ltd</a>
      <ul id="nav-mobile" class="right hide-on-med-and-down">
<li><a href="addBag.php">Add New Bag</a></li>
 <li><a href="viewBag.php">View Bags</a></li>
 <li><a href="sortBag.php">Sort Bags By Order</a></li>
 <li><a href="browseBagByCat.php">Browse Bags By Category</a></li>
 <li><a href="searchBag.php">Search Bags</a></li>
 <li><a href="purchaseReport.php">Purchase Report</a></li>
 <li><a href="bagLogins.php">Logout</a></li>   
      </ul>
 </div> 
  </nav>
  
	<!--Creation of two form boxes to facilitate the sorting procedures-->
  <html>
	<form action = "sortBag.php" method="GET">  
	<button value= "Desc" name = "type">Sort By Highest Price</button>
	<button value= "Asc" name = "type">Sort By Lowest Price</button>
	</form>
	</html>

		<?php
		//Establishing local connection with server and database 
		include_once("errorhandler.php");
		ini_set('error_reporting', E_NOTICE);
		ini_set('error_reporting', E_ERROR);
		error_reporting(0);

		$server="localhost";
		$database="sbags_db";
		$user="root";
		$password="";
		$link=mysql_connect($server,$user,$password);
		if ($link==false){
		echo "error connecting to db";
		exit ();
	}
		if (!mysql_select_db($database,$link)) {
		echo "error selecting database";
		exit();
	}	
		//Query that re-organazies the order of item prices in ascending and descending
		$sort_text = 0;
		if (isset($_REQUEST["type"])){
		$sort_text=$_REQUEST["type"];
		$str_query="select BAG.BAG_ID, BAG_NAME, PRICE from bag,bag_items WHERE BAG.BAG_ID = BAG_ITEMS.BAG_ID order by price $sort_text";
		}else{
		$str_query="select BAG.BAG_ID, BAG_NAME, PRICE from bag,bag_items WHERE BAG.BAG_ID = BAG_ITEMS.BAG_ID";
		}
		$result=mysql_query($str_query, $link);
		
		//Placing data into a table with specified bag variable names
		echo $result;
		if ($result==false){
		echo "error running query";
		exit();
	}
		//Placing data into a table with specified bag variable names
		echo "<table border ='40'>";
		echo "<tr style= 'background-color:orange; color:white; text-align:center'> <td> BAG ID</td><td>BAG NAME</td><td>PRICE</td></tr>";
		while ($row = mysql_fetch_assoc($result)){
		echo "<tr> <td> ".$row['BAG_ID']." </td> <td style ='text-align:left'> ".$row['BAG_NAME']."</td> <td> 
		".$row['PRICE']." </td></tr>";
	}
	mysql_close($link);
?>
			<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
			<script type="text/javascript" src="js/materialize.min.js"></script>
			</body>
			</html>