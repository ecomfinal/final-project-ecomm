<!--Author: Reuben Larmie
	Date:11-04-2016
	Lecturer: Dr. Nathan Amanquah
-->
<!DOCTYPE html>
   <html>
    <head>
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
      <meta name="viewport" content="width=device-width, initial-scale=2.0"/>
    </head>
    <body>
<nav>
    <div class="nav-wrapper amber darken-3">
     <a href="../index.php" class="brand-logo">BEST BAG BUY Ltd</a>
      <ul id="nav-mobile" class="right hide-on-med-and-down">
<li><a href="addBag.php">Add New Bag</a></li>
 <li><a href="viewBag.php">View Bags</a></li>
 <li><a href="sortBag.php">Sort Bags By Order</a></li>
 <li><a href="browseBagByCat.php">Browse Bags By Category</a></li>
 <li><a href="searchBag.php">Search Bags</a></li>
 <li><a href="purchaseReport.php">Purchase Report</a></li>
 <li><a href="bagLogins.php">Logout</a></li>   
      </ul>
 </div> 
  </nav>
	<!--Establishing local connection with server and database-->
<?php
include_once("errorhandler.php");
include("adsq.php");
ini_set('error_reporting', E_NOTICE);
ini_set('error_reporting', E_ERROR);
error_reporting(0);

$start=0;
$limit=5;
 
if(isset($_GET['id']))
{
    $id=$_GET['id'];
    $start=($id-1)*$limit;
}
else{
    $id=1;
}
//Fetch from database first 101 items which is its limit. For that when page open you can see first 10 items. 
$query=mysqli_query($dbconfig,"Select BAG_ID, BAG_NAME, BAG_CAT.BAG_CAT, BAG_BRAND.BAG_BRAND_NAME, YEAR from bag, bag_brand, bag_cat
where bag.bag_cat = bag_cat.bag_cat_id AND bag.bag_brand_id=bag_brand.bag_brand_id GROUP BY BAG_ID LIMIT $start, $limit");
echo "<table border ='30'>";
echo "<tr style= 'background-color:orange; color:white; text-align:center'> <td> BAG ID</td><td>BAG Name</td><td>BAG CATEGORY</td><td>BAG BRAND NAME</td><td>YEAR</td>tr>";
?>
<ol>
<?php
//print 20 items
while($row=mysqli_fetch_array($query))
{
echo "<tr> <td> ".$row['BAG_ID']." </td> 
<td style ='text-align:left'><a href='details.php?id=".$row['BAG_ID']."'>".$row['BAG_NAME']."</a></td>
<td> ".$row['BAG_CAT']." </td>
<td> ".$row['BAG_BRAND_NAME']." </td>
<td> ".$row['YEAR']." </td></tr>";
}
?>
</ol>
<?php
//fetch all the data from database.
$rows=mysqli_num_rows(mysqli_query($dbconfig,"Select BAG_ID, BAG_NAME, BAG_CAT.BAG_CAT, BAG_BRAND.BAG_BRAND_NAME, YEAR from bag, bag_brand, bag_cat
where bag.bag_cat = bag_cat.bag_cat_id AND bag.bag_brand_id=bag_brand.bag_brand_id GROUP BY BAG_ID"));
//calculate total page number for the given table in the database 
$total=ceil($rows/$limit);
if($id>1)
{
    //Go to previous page to show previous 10 items. If its in page 1 then it is inactive
    echo "<a href='?id=".($id-1)."' class='button'> PREVIOUS </a>";
}
if($id!=$total)
{
    ////Go to previous page to show next 10 items.
    echo "<a href='?id=".($id+1)."' class='button'> || NEXT  </a>";
}
?>
<ul class='page'>
<?php
//show all the page link with page number. When click on these numbers go to particular page. 
        for($i=1;$i<=$total;$i++)
        {
            if($i==$id) { echo "<li style= 'display: inline' class='current'>".$i."</li>"; }
             
            else { echo "<li style= 'display: inline'><a href='?id=".$i."'>".$i."</a></li>"; }
        }
				
?>
</ul>
</div>

