<?php

require_once './Twig/Autoloader.php';
Twig_Autoloader::register();

$loader = new Twig_Loader_Filesystem('./templates');
$twig = new Twig_Environment($loader);
$template = $twig->loadTemplate('checkout.html');
$params = array('aName' => 'BEST BAGS BUY');
$template->display($params);
?>