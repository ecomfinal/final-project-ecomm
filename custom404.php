<?php

require_once './Twig/Autoloader.php';
Twig_Autoloader::register();

$loader = new Twig_Loader_Filesystem('./templates');
$twig = new Twig_Environment($loader);
$template = $twig->loadTemplate('custom404.html');
$params = array('pagecontent'=> 'The page is invalid sorry','pagecontent1'=> 'Page unavailable, please visit home page again' );
$template->display($params);
?>