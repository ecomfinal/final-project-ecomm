<?php
session_start();
ini_set('error_reporting', E_NOTICE);
ini_set('error_reporting', E_ERROR);
error_reporting(0);

define("DB_HOST", 'localhost');
define("DB_NAME", 'sbags_db');
define("DB_PORT", 3306);
define("DB_USER", "root");
define("DB_PWORD", "");

//try establish db connection
$mysqli = new mysqli(DB_HOST, DB_USER, DB_PWORD, DB_NAME);

//check connection
if ($mysqli->connect_error) {
    die("Connection Failed :: " . $mysqli->connect_error);
}

if (isset($_GET['bag_id']) && !isset($_GET['rmv'])){
	$bid = (int) $_GET['bag_id'];
	$sqlQuery = "SELECT bag_name, cost FROM bag_table where bag_id = $bid";
	$sqlStmt = $mysqli->prepare($sqlQuery);
	if ($sqlStmt === FALSE) {
		echo
		'Wrong SQL :: ' . $sqlQuery. 'Error:' . $mysqli->error;
		}
	$sqlStmt->execute();
	$sqlStmt->bind_result($tempName, $tempCost);
	if ($sqlStmt->fetch()){
		$bname = $tempName;
		$bcost = $tempCost;
	}
	
	if (!isset($_SESSION['bag_table'])){
		$_SESSION['bag_table'] = array ();
		$_SESSION['bag_table'][] = array ('bag_id' => $bid, 'bag_name' => $bname, 'cost' => $tempCost, 'count' =>1);
		
	} elseif (isset($_SESSION['bag_table'])){
		$increased = 0;
		foreach ($_SESSION['bag_table'] as &$item) {
			if ($item['bag_id'] === $bid) {
				$item['count'] ++;
				$increased = 1;
				break;
			}
		}
		unset ($item);
		if ($increased ===0){
			$_SESSION['bag_table'][] = array('bag_id' => $bid, 'bag_name' => $bname, 'cost' => $tempCost, 'count' => 1);
		}
	}
	$sqlStmt ->close();
}

//starting pagination code
if (isset($_REQUEST['page'])) {
    $pageno = (int) $_REQUEST['page'];
} else {
    $pageno = 1;
}

//prepared statement for query
$csql = "SELECT count(*) FROM bag_table";

$cstmt = $mysqli->prepare($csql);
if ($cstmt === FALSE) {
    echo
    'Wrong SQL :: ' . $sql . 'Error:' . $mysqli->error;
}
$cstmt->execute();
$cstmt->bind_result($count);
if ($cstmt->fetch()) {
    $cdata = $count;
}

$rows_per_page = 5;
$lastpage = ceil($cdata / $rows_per_page);

if ($pageno > $lastpage) {
    $pageno = $lastpage;
}

if ($pageno < 1) {
    $pageno = 1;
}

$limit = "LIMIT " . ($pageno - 1) * $rows_per_page . " , " . $rows_per_page;

$cstmt->close();

$sql = " SELECT * FROM bag_table " . $limit;

$stmt = $mysqli->prepare($sql);
if ($stmt === FALSE) {
    echo
    'Wrong SQL :: ' . $sql . 'Error:' . $mysqli->error;
}
$stmt->execute();
$stmt->bind_result($bag_id, $bag_name, $bag_cat, $year, $cost, $bag_brand_name, $pictures);
$data = array();
while ($stmt->fetch()) {
    $data[] = array('bag_id' => $bag_id, 'bag_name' => $bag_name, 'bag_cat' => $bag_cat, 'year' => $year, 'cost' => $cost, 'bag_brand_name' => $bag_brand_name, 'pictures' => $pictures);
}
$stmt->close();
$mysqli->close();





require_once './Twig/Autoloader.php';
Twig_Autoloader::register();

$loader = new Twig_Loader_Filesystem('./templates');
$twig = new Twig_Environment($loader);
$template = $twig->loadTemplate('index.html');
$params = array('aName' => 'HOME', 'bags' => $data, 'pageno' => $pageno, 'lastpage' => $lastpage, 'cart' => $_SESSION['bag_table']);
$template->display($params);
?>