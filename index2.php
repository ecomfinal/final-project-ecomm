<?php

require_once './Twig/Autoloader.php';
Twig_Autoloader::register();

$loader = new Twig_Loader_Filesystem('./templates');
$twig = new Twig_Environment($loader);
$template = $twig->loadTemplate('index2.html');
$params = array('secondName' => 'The Best Bags TO Be Held Around!');
$template->display($params);

?>