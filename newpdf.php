<?php
session_start();
require('fpdf.php');	
$pdf	=	new	FPDF();	
$pdf->AddPage();	
$pdf->SetFont('Arial','b', 14);

define("DB_HOST", 'localhost');
define("DB_NAME", 'jackets_home');
define("DB_PORT", 3306);
define("DB_USER", "root");
define("DB_PWORD", "");
$myResult="";
$str_query="";

$mySQLi=mysqli_connect(DB_HOST, DB_USER, DB_PWORD, DB_NAME);

if(mysqli_connect_errno()){

	printf("Connection has Failed: %s\n", mysqli_connect_error());
	exit();
}

$str_query = "SELECT * FROM orders_record
WHERE DATE(order_date)=CURDATE()
ORDER BY order_date DESC";

	//$str_query = "SELECT * FROM orders_record";

$myResult= mysqli_query($mySQLi, $str_query);
$sum=0;
$orders = array();
while($myRow=mysqli_fetch_assoc($myResult)){
	$orders[]=array('ordId'=>$myRow['order_id'], 'cusId'=>$myRow['customer_id'], 'qty'=>$myRow['quantity'], 'amount'=>$myRow['amount'], 'shipadd'=>$myRow['shipping_address'], 'deliverydate'=>$myRow['delivery_date']);
$sum=$sum+$myRow['amount'];
}

$pdf->Cell(0,10,'End of Day Orders Report', 0,1,'C');	
// Table Heading
$headings=array("OrderId","CustomerId", "Qty", "Amt", "ShipAdd", "deliverydate");
foreach($headings	as	$col)	
	$pdf->Cell(30,7,$col,1);	
$pdf->Ln();	

	//Data	
foreach($orders	as	$row)	
{	
	foreach($row	as	$col)	
		$pdf->Cell(30,6,$col,1);	
	$pdf->Ln();	
}
$pdf->Cell(30,6,'Total',1,'LB');	
$pdf->Cell(30,6,$sum,1,1,'LB');	
// $pdf->Cell(25,20,'Total',1,'LB');
// $pdf->Cell(30,20,$total,1,1,'LB');
	$pdf->Ln();


$pdf->Output();

?>