<?php
session_start();
require('fpdf.php');	
$pdf	=	new	FPDF();	
$pdf->AddPage();	
$pdf->SetFont('Arial','b', 14);

if(isset($_SESSION['shopping'])){	
$shopping=$_SESSION['shopping'];
$total=$_SESSION['total'];
if(isset($_SESSION['name'])){
$name=$_SESSION['name'];
$pdf->Cell(0,10,'Customer Name:'.$name,0,1,'C');	
}
else
$pdf->Cell(0,10,'Customer Name: Not Registered',0,1,'C');
$pdf->Ln();
$count=count($shopping);
// put header here ->name, address


$pdf->Cell(45,20,'Name',1,'LB');
$pdf->Cell(30,20,'Category',1,'LB');
$pdf->Cell(15,20,'Qty',1,'LB');
$pdf->Cell(25,20,'Price',1,'LB');
$pdf->Cell(30,20,'Amount',1,1,'LB');
$pdf->SetFont('Arial','i', 14);
for($i=0;$i<$count;$i++){
	$pdf->Cell(45,20,$shopping[$i]['pname'],1,'LB');
	$pdf->Cell(30,20,$shopping[$i]['category'],1,'LB');
	$pdf->Cell(15,20,$shopping[$i]['quantity'],1,'LB');
	$pdf->Cell(25,20,$shopping[$i]['price'],1,'LB');
	$pdf->Cell(30,20,$shopping[$i]['amount'],1,1,'LB');
}
$pdf->SetFont('Arial','b', 14);
$pdf->Cell(45,20,'',1,'LB');
$pdf->Cell(30,20,'',1,'LB');
$pdf->Cell(15,20,'',1,'LB');
$pdf->Cell(25,20,'Total',1,'LB');
$pdf->Cell(30,20,$total,1,1,'LB');
}
$pdf->Output();	

?>