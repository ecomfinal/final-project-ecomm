<!--Author: Reuben Larmie
	Date:11-04-2016
	Lecturer: Dr. Nathan Amanquah
-->
<!DOCTYPE html>
	<?php
	
	session_start();
	if (isset($_SESSION['user_name'])){
	}
	else {
	//Placing header into link page for redirection
	header("Location: http://localhost/ecomfinal/admin/bagLogins.php");
	}
?> 
  <html>
    <head>
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link type="text/css" rel="stylesheet" href="css1/materialize.min.css"  media="screen,projection"/>
      <meta name="viewport" content="width=device-width, initial-scale=2.0"/>
    </head>
    <body>
<nav>
<!--Form creation bearing various parameters with header form-->
    <div class="nav-wrapper amber darken-3">
       <a href="index.php" class="brand-logo">BEST BAG BUY Ltd</a>
      <ul id="nav-mobile" class="right hide-on-med-and-down">
 <li><a href="http://localhost/ecomfinal/admin/addBag.php">Add Bags</a></li>
 <li><a href="http://localhost/ecomfinal/admin/viewBag.php">View Bags</a></li>
 <li><a href="http://localhost/ecomfinal/admin/sortBag.php">Sort Bags By Order</a></li>
 <li><a href="http://localhost/ecomfinal/admin/browseBagByCat.php">Browse Bags By Category</a></li>
 <li><a href="http://localhost/ecomfinal/admin/searchBag.php">Search Bags</a></li>
 <li><a href="http://localhost/ecomfinal/admin/purchaseReport.php">Purchase Report</a></li>
<li><a href="http://localhost/ecomfinal/newpdf.php">Purchase pdf Report</a></li>
 <li><a href="http://localhost/ecomfinal/admin/bagLogout.php">Logout</a></li>   

      </ul>
 </div> 
  </nav>
  
  <?php
	//Establishing local connection with server and database 
	$server="localhost";
	$database="sbags_db";
	$user="root";
	$password="";
	$link=mysql_connect($server,$user,$password);
	if ($link==false){
	echo "error connecting to db";
	exit ();
	}
	if (!mysql_select_db($database,$link)) {
	echo "error selecting database";
	exit();
	}
	$str_query="select CUSTOMERS.CUST_ID, SURNAME, FIRSTNAME, SUM(qty), SUM(price), MAX(order_id) FROM customers, bag_items Group by customers.cust_id";
	$result=mysql_query($str_query, $link);
	echo $result;
	if ($result==false){
	echo "error running query";
	exit();
	}
	//Placing data into a table with specified bag variable names
	echo "<table border ='40'>";
	echo "<tr style= 'background-color:orange; color:white; text-align:center'> <td> CUSTOMER ID</td><td>CUSTOMER SURNAME</td><td>CUSTOMER FIRSTNAME</td>
	<td>TOTAL QUANTITY</td><td>TOTAL PRICE</td><td>ORDER_ID</td>tr>";
	while ($row = mysql_fetch_assoc($result)){
	echo "<tr> <td> ".$row['CUST_ID']." </td> 
	<td style ='text-align:left'>".$row['SURNAME']."</a></td>
	<td> ".$row['FIRSTNAME']." </td>
	<td> ".$row['SUM(qty)']." </td>
	<td> ".$row['SUM(price)']." </td>
	<td> ".$row['MAX(order_id)']." </td></tr>";
	} 
	mysql_close($link);
?>
     
      <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
      <script type="text/javascript" src="js1/materialize.min.js"></script>
    </body>
  </html>