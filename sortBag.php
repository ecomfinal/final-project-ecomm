<!--Author: Reuben Larmie
	Date:11-04-2016
	Lecturer: Dr. Nathan Amanquah
-->
<!DOCTYPE html>
    <html>
    <head>
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link type="text/css" rel="stylesheet" href="css1/materialize.min.css"  media="screen,projection"/>
      <meta name="viewport" content="width=device-width, initial-scale=2.0"/>
    </head>
    <body>
<nav>
	<!--Creating a form page to hold the parameters for administrator view side-->
    <div class="nav-wrapper amber darken-3">
       <a href="index.php" class="brand-logo">BEST BAG BUY Ltd</a>
      <ul id="nav-mobile" class="right hide-on-med-and-down">
 <li><a href="http://localhost/ecomfinal/admin/addBag.php">Add Bags</a></li>
 <li><a href="http://localhost/ecomfinal/admin/viewBag.php">View Bags</a></li>
 <li><a href="http://localhost/ecomfinal/admin/sortBag.php">Sort Bags By Order</a></li>
 <li><a href="http://localhost/ecomfinal/admin/browseBagByCat.php">Browse Bags By Category</a></li>
 <li><a href="http://localhost/ecomfinal/admin/searchBag.php">Search Bags</a></li>
 <li><a href="http://localhost/ecomfinal/admin/purchaseReport.php">Purchase Report</a></li>
 <li><a href="http://localhost/ecomfinal/admin/bagLogout.php">Logout</a></li>   
      </ul>
 </div> 
  </nav>
  
	<!--Creation of two form boxes to facilitate the sorting procedures-->
  <html>
	<form action = "sortBag.php" method="GET">  
	<button value= "Desc" name = "type">Sort By Highest Price</button>
	<button value= "Asc" name = "type">Sort By Lowest Price</button>
	</form>
	</html>

		<?php
		//Establishing local connection with server and database 
		$server="localhost";
		$database="sbags_db";
		$user="root";
		$password="";
		$link=mysql_connect($server,$user,$password);
		if ($link==false){
		echo "error connecting to db";
		exit ();
	}
		if (!mysql_select_db($database,$link)) {
		echo "error selecting database";
		exit();
	}	
		//Query that re-organazies the order of item prices in ascending and descending
		$sort_text = 0;
		if (isset($_REQUEST["type"])){
		$sort_text=$_REQUEST["type"];
		$str_query="select BAG.BAG_ID, BAG_NAME, PRICE from bag,bag_items WHERE BAG.BAG_ID = BAG_ITEMS.BAG_ID order by price $sort_text";
		}else{
		$str_query="select BAG.BAG_ID, BAG_NAME, PRICE from bag,bag_items WHERE BAG.BAG_ID = BAG_ITEMS.BAG_ID";
		}
		$result=mysql_query($str_query, $link);
		
		//Placing data into a table with specified bag variable names
		echo $result;
		if ($result==false){
		echo "error running query";
		exit();
	}
		//Placing data into a table with specified bag variable names
		echo "<table border ='40'>";
		echo "<tr style= 'background-color:orange; color:white; text-align:center'> <td> BAG ID</td><td>BAG NAME</td><td>PRICE</td></tr>";
		while ($row = mysql_fetch_assoc($result)){
		echo "<tr> <td> ".$row['BAG_ID']." </td> <td style ='text-align:left'> ".$row['BAG_NAME']."</td> <td> 
		".$row['PRICE']." </td></tr>";
	}
	mysql_close($link);
?>
			<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
			<script type="text/javascript" src="js1/materialize.min.js"></script>
			</body>
			</html>